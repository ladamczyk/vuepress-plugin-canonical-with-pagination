# Vuepress Plugin Canonical with pagination

Add  `<link rel='canonical' ...>`, `<link rel='next' ...>` / `<link rel='prev' ...>` to your vuepress site. Project extends [vuepress-plugin-canonical](https://github.com/IOriens/vuepress-plugin-canonical)

## Install

```
npm i vuepress-plugin-canonical-with-pagination -D
```

## Usage

Read [How to use vuepress Plugin](https://v1.vuepress.vuejs.org/plugin/using-a-plugin.html) first, modify your `.vuepress/config.js`.
```js
module.exports = {
  plugins: [
    [
      'vuepress-plugin-canonical-with-pagination',
      {
        baseURL: 'https://google.com', // base url for your schema, mandatory, default: ''
      }
    ]
  ]
}
```

## Refrences

- [add extra tags to <head> per page](https://github.com/vuejs/vuepress/issues/894)
- [globalUIComponents](https://v1.vuepress.vuejs.org/plugin/option-api.html#globaluicomponents)