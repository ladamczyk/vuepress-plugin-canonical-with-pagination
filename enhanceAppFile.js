import LinkRef from './LinkRef.vue'

export default ({ Vue }) => {
  Vue.component('LinkRef', LinkRef)
}
