const { path } = require('@vuepress/shared-utils')

module.exports = (options = {}, context) => {
  const baseUrl = options.baseURL || ''

  return {
    define () {
      return {
        BASE_URL: baseUrl
      }
    },
    enhanceAppFiles: path.resolve(__dirname, 'enhanceAppFile.js'),
    globalUIComponents: ['LinkRef'],
  }
}
